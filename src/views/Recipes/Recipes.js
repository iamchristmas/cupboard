import React, {Component} from 'react';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Table } from 'reactstrap';
import { AppSwitch } from '@coreui/react'

class Recipes extends Component{

    render(){
    return(
        <div className="animated fadeIn">
            <Row>
                <CardBody>
                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                <thead className="thead-light">
                <tr>
                    <th>Dish</th>
                    <th>Time To Cook</th>
                    <th>Difficulty</th>
                    <th>Diet</th>
                    <th>Cuisine</th>
                    <th>Favorited</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                <tr>
                    <td>Tacos Al Pastor</td>
                    <td>1 Hour</td>
                    <td>Low</td>
                    <td>Omnivorous</td>
                    <td><i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i></td>
                    <td><i className="icon-star"></i></td>
                </tr>
                </tbody>
                </Table>
            </CardBody>
        </Row>
        <Row>
        <Col align="center">
            <Button block color="primary" className="btn-pill">
                <i className="icon-settings"></i> 
            </Button>
            </Col>
            <Col class="col-xs-1 center-block">
            <div align="center">
            <nav aria-label="..." >
            <ul class="pagination">
                <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                <a class="page-link" href="#">Next</a>
                </li>
            </ul>
            </nav>     
            </div>
            </Col>    
            <Col align="center">
                <form class="form-inline">
                <input class="form-control" type="search" placeholder="Search" aria-label="Search" /> 
                <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
            </form>
            </Col >   

        </Row>
    </div>
    );
    }
}
export default Recipes;