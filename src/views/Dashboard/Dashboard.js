import React, { Component, lazy, Suspense } from 'react';
import { Bar, Line } from 'react-chartjs-2';
import {
  Badge,  Button,  ButtonDropdown,  ButtonGroup,  ButtonToolbar,  Card,  CardBody,  CardFooter,  CardHeader,  CardTitle,  Col,  Dropdown,  DropdownItem,  DropdownMenu,  DropdownToggle,  Progress,  Row,  Table,  Jumbotron,
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'



const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')




class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
          <Card>
            <Jumbotron>
              <h1 className="display-3"> Welcome to the Pantry Pal! </h1>
              <p classname="lead">This project was created by Christopher Smith for the Microsoft Software and Systems Academy and is undergoing active development.</p>
              <hr className="my-2" />
              <p>For more projects like this, check out my Github!</p>
              <p className="lead">
              <button class="btn-github btn-brand mr-1 mb-1 btn btn-secondary" onClick={event =>  window.location.href='https://www.github.com/iamchristmas'}>
                <i class="fa fa-github"></i><span>iamchristmas</span>
                </button>
              </p>
            </Jumbotron>
          </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              {/* <CardHeader>
                Traffic {' & '} Sales
              </CardHeader> */}
              <CardBody>
                <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                  <thead className="thead-light">
                  <tr>
                    <th > Recipe</th>
                    <th>Ingredients</th>
                    <th className="text-center">Country of Origin</th>
                    <th ><i className="icon-clock"></i> Time To Cook</th>
                    <th>Activity</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td >
                      Tacos Al Pastor
                    </td>
                    <td>
                      <div>Pork Loin, White Onion, Pineapple.... </div>
                    </td>
                    <td className="text-center">
                      <i className="flag-icon flag-icon-mx h4 mb-0" title="pl" id="pl"></i>
                    </td>
                    <td>
                    <span class="badge badge-danger">1 Hour</span>
                    </td>
                  </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Dashboard;
