import React, {Component} from 'react';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Table, Pagination } from 'reactstrap';
import { AppSwitch } from '@coreui/react'

class Inventory extends Component{

        render(){
        return(
            <div className="animated fadeIn">
                <Row>
                    <CardBody>
                    <Table hover responsive className="table-outline mb-0 d-none d-sm-table">
                    <thead className="thead-light">
                    <tr>
                        <th>Select</th>
                        <th>Ingredient</th>
                        <th>Type</th>
                        <th>Quantity</th>
                        <th>Matching Recipes</th>
                        <th>Date Added</th>
                        <th>Estimated Expiration</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline /></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline /></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline /></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline /></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline /></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline /></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline /></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline checked/></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline checked/></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline checked/></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    <tr>
                        <td><AppSwitch className={'mx-1'}  color={'dark'} outline checked/></td>
                        <td> Pork Loin </td>
                        <td>Protein</td>
                        <td>1 Kg</td>
                        <td>Tacos Al Pastor</td>
                        <td>March 15th, 2020</td>
                        <td>March 30th, 2020</td>
                    </tr>
                    </tbody>
                    </Table>
                </CardBody>
            </Row>
            <Row>
            <Col>
                <Button block color="primary" className="btn-pill">
                    <i className="icon-plus"></i> 
                </Button>
            </Col>
            <nav aria-label="..." >
            <ul class="pagination">
                <li class="page-item disabled">
                <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active">
                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                <a class="page-link" href="#">Next</a>
                </li>
            </ul>
            </nav>
            <Col>
                <Button block color="primary" className="btn-pill">
                    <i className="icon-minus"></i> 
                </Button>
            </Col>
            </Row>
        </div>
        );
    }
}

export default Inventory;