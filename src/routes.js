import React from 'react';


const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Inventory = React.lazy(() => import('./views/Inventory'));
const Recipes = React.lazy(() => import('./views/Recipes/Recipes'));
const ShoppingCart = React.lazy(()=> import('./views/ShoppingCart'));
const Users = React.lazy(() => import('./views/Users'));
const User = React.lazy(() => import('./views/Users/User'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  { path: '/inventory', exact: true, name: 'Inventory', component: Inventory },
  { path: '/recipes', exact: true, name: 'Recipes', component: Recipes },
  { path: '/shoppingcart', exact: true, name: 'ShoppingCart', component: ShoppingCart},
];

export default routes;
