#Cupboard

The cupboard is the web-based frontend for my [ThePantryApp Project](https://www.github.com/iamchristmas/ThePantryApp.git) and leverages React to generate an intuitive SPA.

This project is undergoing active development and a static version of this page is viewable as an Azure App service [here](https://cupboard-dev.azurewebsites.net/)